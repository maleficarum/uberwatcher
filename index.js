var bunyan = require('bunyan'),
    http = require('https');

var log = bunyan.createLogger({name: "UberWatcher",src: false});
var start = {}, end = {}, token, price = {}, time = {}, callback;

module.exports = {
  configure: function(key, startLat, startLon, endLat, endLon, cback) {
    start.latitude = startLat;
    start.longitude = startLon;

    end.latitude = endLat;
    end.longitude = endLon;

    token = key;

    callback = cback;

    price = {
      host: 'api.uber.com',
      path: '/v1/estimates/price?start_latitude=' + start.latitude + '&start_longitude=' + start.longitude + '&end_latitude=' + end.latitude + '&end_longitude=' + end.longitude,
      port: '443',
      headers: {'Authorization': 'Token ' + token},
      method : "GET"
    };

    log.info("Watching for ", {"start": start, "end": end, "price": price});
  },
  price: function() {
      var req = http.get(price, function(res) {
        log.debug('STATUS: ' + res.statusCode);

        res.on('data', function(data) {
          callback(JSON.parse(data));
        }).on('end', function() {
        })
      });

      req.on('error', function(e) {
        log.error(e.message);
      });
  }
};
